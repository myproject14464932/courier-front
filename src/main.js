import {createApp} from 'vue'
import App from './App.vue'
import router from "@/plugins/router"
import store from "@/plugins/vuex/store";
import i18n from "@/plugins/i18n/i18n";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from '@fortawesome/fontawesome-svg-core';
import {faInstagramSquare, faLine, faTwitter,} from "@fortawesome/free-brands-svg-icons";
import {
    faAngleLeft,
    faAngleRight,
    faCaretDown,
    faPlus,
    faRightFromBracket,
    faUser,
    faUserPen,
    faCircleArrowLeft,
    faCircleExclamation
} from "@fortawesome/free-solid-svg-icons";

library.add(
    faInstagramSquare,
    faLine,
    faTwitter,
    faAngleRight,
    faAngleLeft,
    faCaretDown,
    faPlus,
    faRightFromBracket,
    faUser,
    faUserPen,
    faCircleArrowLeft,
    faCircleExclamation

)

createApp(App)
    .use(store)
    .use(router)
    .use(i18n)
    .component('font-awesome-icon', FontAwesomeIcon)
    .mount('#app')
