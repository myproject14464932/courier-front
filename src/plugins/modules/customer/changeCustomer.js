import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        putCustomer(context, data) {
            return new Promise((resolve, reject) => {
                axios.put(
                    'customers/' + data.id,
                    {
                        givenName: data.givenName,
                        familyName: data.familyName,
                        phone: data.phone,
                    }

                )
                    .then((response) => {
                        console.log("Customer changed")
                        console.log(response)

                        resolve()
                    })
                    .catch(() => {
                        console.log("Something went wrong while taking customer")
                        reject()
                    })
            })
        }
    }
}