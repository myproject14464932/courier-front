import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        deleteCustomer(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete('customers/' + id)
                    .then(() => {
                        console.log("Customer deleted")
                        resolve()
                    })
                    .catch(() => {
                        console.log("Something went wrong while deleting customer")
                        reject()
                    })
            })
        }
    }
}