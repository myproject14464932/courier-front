import axios from "@/plugins/vuex/axios";

export default {
    actions: {
            createCourier(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('couriers', data)
                    .then((response) => {
                        console.log('Created new Courier')
                        console.log(response)

                        let data = {
                            id: response.data.id,
                            givenName: response.data.givenName,
                            familyName: response.data.familyName,
                            image: response.data.image,
                            phone: response.data.phone,
                            email: response.data.email,
                            type: response.data.type,
                            serialNumber: response.data.serialNumber,
                            stateNumber: response.data.stateNumber,
                            carImage: response.data.carImage

                        }

                        context.commit('updateCreatedCourier', data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Courier Not Created!')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateCreatedCourier(state, data) {
            state.createdCourier = data
        }
    },
    state: {
        createdCourier: {
            id: null,
            givenName: null,
            familyName: null,
            image: null,
            phone: null,
            email: null,
            type: null,
            serialNumber: null,
            stateNumber: null,
            carImage: null,

        }
    },
    getters: {
        getCreatedCourier(state) {
            return state.createdCourier
        }
    },
}
