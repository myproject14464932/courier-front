import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        createCustomer(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('customers', data)
                    .then((response) => {
                        console.log('Created new Customer')
                        console.log(response)

                        let data = {
                            id: response.data.id,
                            email: response.data.email,
                            givenName: response.data.givenName,
                            familyName: response.data.familyName,
                            phone: response.data.phone
                        }

                        context.commit('updateCreatedCustomer', data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Customer Not Created!')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateCreatedCustomer(state, data) {
            state.createdCustomer = data
        }
    },
    state: {
        createdCustomer: {
            id: null,
            email: null,
            givenName: null,
            familyName: null,
            phone: null,
        }
    },
    getters: {
        getCreatedCustomer(state) {
            return state.createdCustomer
        }
    },
}
