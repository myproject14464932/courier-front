import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        isUniqueEmail(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('users/is_unique_email', data)
                    .then((response) => {
                        console.log(response)
                        console.log(response.data.isUnique)

                        let data = {
                            email: response.data.isUnique
                        }

                        context.commit('updateEmail', data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Error getting email')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateEmail(state, data) {
            state.isUniqueEmail = data
        }
    },
    state: {
        isUniqueEmail: {
            email: '',
        }
    },
    getters: {
        getIsUniqueEmail(state) {
            return state.isUniqueEmail
        }
    },
}
