import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        AboutMe(context) {
            return new Promise((resolve, reject) => {
                axios.get('users/about_me')
                    .then((response) => {
                        console.log(response)

                        let user = {
                            id: response.data.customers[0].id,
                            givenName: response.data.customers[0].givenName,
                            familyName: response.data.customers[0].familyName,
                            phone: response.data.customers[0].phone,
                            userId: response.data.id,
                        }

                        context.commit('updateUser', user)
                        resolve()
                    })
                    .catch(() => {
                        console.log('error')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateUser(state, data) {
            state.user = data
        }
    },
    state: {
        user: {}
    },
    getters: {
        getAboutMe(state) {
            return state.user
        }
    }
}

