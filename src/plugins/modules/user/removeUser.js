import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        deleteUser(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete('users/' + id)
                    .then(() => {
                        console.log("User deleted")
                        context.commit('updateToken', null)
                        context.commit('updateToken2', null)
                        resolve()

                    })
                    .catch(() => {
                        console.log("Something went wrong while deleting user")
                        reject()
                    })
            })
        }
    },
    // mutations: {
    //     updateToken(state, accessToken) {
    //         state.token = accessToken
    //         localStorage.setItem('accessToken', accessToken);
    //     },
    //     updateToken2(state, refreshToken ) {
    //         state.tokenRefresh = refreshToken
    //         localStorage.setItem('refreshToken', refreshToken);
    //
    //     }
    // },
}