import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        fetchToken(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('users/auth', data)
                    .then((response) => {
                        console.log('Token received')
                        console.log(response)

                        context.commit('updateToken', response.data.accessToken)
                        context.commit('updateToken2', response.data.refreshToken)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Error getting token')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateToken(state, accessToken) {
            localStorage.setItem('accessToken', accessToken)

            state.token = accessToken
        },
        updateToken2(state, refreshToken ) {
            localStorage.setItem('refreshToken', refreshToken)

            state.tokenRefresh = refreshToken

        }
    },
    state: {
        token: localStorage.getItem('accessToken'),
        tokenRefresh: localStorage.getItem('refreshToken'),
    },
    getters: {
        getToken(state) {
            return state.token
        },
        getAuthorize(state) {
            return state.token !== null
        }
    },
}
