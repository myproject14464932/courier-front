import axios from "axios";

export default {
    actions: {
        pushMediaObject(context, data) {
            const file = new FormData()
            file.append('file', data)

            return new Promise((resolve, reject) => {
                axios.post('/media_objects', file)
                    .then((response) => {
                        console.log('Photo created')

                        context.commit('updateFile', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Photo NOT created!')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateFile(state, data) {
            state.createdFile = data
        }
    },
    state: {
        createdFile: {}
    },
    getters: {
        getCreatedFile(state) {
            return state.createdFile
        }
    }
}
