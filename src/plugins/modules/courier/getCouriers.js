import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        fetchCouriers(context, url) {
            return new Promise((resolve, reject) => {
                axios.get('couriers?' + url)
                    .then((response) => {
                        console.log('Couriers taken')
                        console.log(response)

                        let couriers = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateCouriers', couriers)
                        resolve()
                    })
                    .catch(() => {
                        console.log('While taking couriers something went wrong')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateCouriers(state, data) {
            state.couriers = data
        }
    },
    state: {
        couriers: {
            models: null,
            totalItems: 0
        }
    },
    getters: {
        getCouriers(state) {
            return state.couriers.models
        }
    }
}