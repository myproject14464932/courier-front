import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        deleteCourier(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete('couriers/' + id)
                    .then(() => {
                        console.log("Courier deleted")
                        resolve()
                    })
                    .catch(() => {
                        console.log("Something went wrong while deleting couriers")
                        reject()
                    })
            })
        }
    }
}