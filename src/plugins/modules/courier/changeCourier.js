import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        putCourier(context, data) {
            return new Promise((resolve, reject) => {
                axios.put(
                    'couriers/' + data.id,
                    {
                        givenName: data.givenName,
                        familyName: data.familyName,
                        phone: data.phone,
                        image: data.image,
                    }

                )
                    .then((response) => {
                        console.log("Couriers changed")
                        console.log(response)

                        resolve()
                    })
                    .catch(() => {
                        console.log("Something went wrong while taking couriers")
                        reject()
                    })
            })
        }
    }
}