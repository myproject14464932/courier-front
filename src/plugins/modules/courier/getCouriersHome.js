import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        fetchCouriersHome(context) {
            return new Promise((resolve, reject) => {
                axios.get('couriers/home')
                    .then((response) => {
                        console.log('Couriers taken')
                        console.log(response)

                        let couriers = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateCouriersHome', couriers)
                        resolve()
                    })
                    .catch(() => {
                        console.log('While taking couriers something went wrong')
                        reject()
                    })
            })
        }
    },

    mutations: {
        updateCouriersHome(state, data) {
            state.couriers = data
        }
    },
    state: {
        couriers: {
            models: null,
            totalItems: 0
        }
    },
    getters: {
        getCouriersHome(state) {
            return state.couriers.models
        }
    }
}