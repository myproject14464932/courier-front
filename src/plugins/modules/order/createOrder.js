import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        createOrder(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('orders', data)
                    .then((response) => {
                        console.log('Created new Order')
                        console.log(response)

                        let data = {
                            title: response.data.title,
                            cargoType: response.data.cargoType,
                            weight: response.data.weight,
                            size: response.data.size,
                            whereFrom: response.data.whereFrom,
                            whereTo:response.data.whereTo,
                            untilWhen:response.data.untilWhen,
                            status:response.data.status,
                            user: {
                                id: response.data.user.id
                            }
                        }

                        context.commit('updateCreatedOrder', data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Order Not Created!')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateCreatedOrder(state, data) {
            state.createdOrder = data
        }
    },
    state: {
        createdOrder: {
            title: null,
            cargoType: null,
            weight: null,
            size: null,
            whereFrom: null,
            whereTo: null,
            untilWhen: null,
            status: null,
            user: {
                id:null,
            },

        }
    },
    getters: {
        getCreatedOrder(state) {
            return state.createdOrder
        }
    },
}
