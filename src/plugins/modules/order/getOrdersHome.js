import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        fetchOrdersHome(context) {
            return new Promise((resolve, reject) => {
                axios.get('orders/home')
                    .then((response) => {
                        console.log('Orders taken')
                        console.log(response)

                        let orders = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateOrdersHome', orders)
                        resolve()
                    })
                    .catch(() => {
                        console.log('While taking orders something went wrong')
                        reject()
                    })
            })
        }
    },

    mutations: {
        updateOrdersHome(state, data) {
            state.orders = data
        }
    },
    state: {
        orders: {
            models: null,
            totalItems: 0
        }
    },
    getters: {
        getOrdersHome(state) {
            return state.orders.models
        }
    }
}