import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        fetchOrders(context, url) {
            return new Promise((resolve, reject) => {
                axios.get('orders?' + url)
                    .then((response) => {
                        console.log('Orders taken')
                        console.log(response)

                        let orders = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateOrders', orders)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Something went wrong while taking orders')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateOrders(state, data) {
            state.orders = data
        }
    },
    state: {
        orders: {
            models: null,
            totalItems: 0,
        }
    },
    getters: {
        getOrders(state) {
            return state.orders.models
        }
    }
}