import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        putOrder(context, data) {
            return new Promise((resolve, reject) => {
                axios.put(
                    'orders/' + data.id,
                    {
                        whereFrom: data.whereFrom,
                        whereTo: data.whereTo,
                        cargoType: data.cargoType,
                        status: data.status
                    }
                    )
                    .then((response) => {
                        console.log("Orders changed")
                        console.log(response)

                        resolve()
                    })
                    .catch(() => {
                        console.log("Something went wrong while taking orders")
                        reject()
                    })
            })
        }
    }
}