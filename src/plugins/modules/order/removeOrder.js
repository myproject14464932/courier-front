import axios from "@/plugins/vuex/axios";


export default {
    actions: {
        deleteOrder(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete('orders/' + id)
                    .then(() => {
                        console.log("Order deleted")
                        resolve()
                    })
                    .catch(() => {
                        console.log("Something went wrong while deleting order")
                        reject()
                    })
            })
        }
    }
}