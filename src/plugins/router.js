import {createRouter, createWebHistory} from "vue-router";
import store from "@/plugins/vuex/store";

const ifNotAuthorized = (to, from, next) => {
    if (!store.getters.getAuthorize) {
        next()
    } else {
        next()
    }
}

const ifAuthorized = (to, from, next) => {
    if (store.getters.getAuthorize) {
        next()
    } else {
        next('/sign-in')
    }
}


const routes = [
    {
        path: '/',
        component: () => import('@/pages/HomePage.vue'),
        beforeEnter: ifAuthorized
    },
    {
        path: '/sign-in',
        name: 'LoginPage',
        component: () => import('@/pages/LoginPage'),
        meta: {layout: 'DefaultLayout'},
        beforeEnter: ifNotAuthorized
    },
    {
        path: '/sign-up-customer',
        name: 'SignUpCustomer',
        component: () => import('@/pages/SignUpCustomer.vue'),
        meta: {layout: 'DefaultLayout'},
        beforeEnter: ifNotAuthorized
    },
    {
        path: '/sign-up-courier',
        component: () => import('@/pages/SignUpCourier'),
        meta: {layout: 'DefaultLayout'},
        beforeEnter: ifNotAuthorized
    },
    {
        path: '/courier',
        component: () => import('@/pages/CouriersPage'),
        meta: {layout: 'BasicLayout'},
        beforeEnter: ifAuthorized
    },
    {
        path: '/order',
        component: () => import('@/pages/CreateOrder'),
        meta: {layout: 'BasicLayout'},
        beforeEnter: ifAuthorized
    },
    {
        path: '/orders-page',
        component: () => import('@/pages/OrdersPage.vue'),
        meta: {layout: 'BasicLayout'},
        beforeEnter: ifAuthorized
    },
    {
        path: '/edit-order',
        component: () => import('@/pages/order/EditOrder.vue'),
        meta: {layout: 'BasicLayout'},
        beforeEnter: ifAuthorized
    },
    {
        path: '/edit-courier',
        component: () => import('@/pages/courier/EditCourier.vue'),
        meta: {layout: 'BasicLayout'},
        beforeEnter: ifAuthorized
    },
    {
        path: '/edit-customer',
        component: () => import('@/pages/customer/EditCustomer.vue'),
        meta: {layout: 'DefaultLayout'},
        beforeEnter: ifAuthorized
    },
]


export default createRouter({
    history: createWebHistory(),
    routes
})