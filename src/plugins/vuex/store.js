import {createStore} from "vuex";
import createCustomer from "@/plugins/modules/registration/createCustomer";
import createMediaObject from "@/plugins/modules/mediaObject/createMediaObject";
import createCourier from "@/plugins/modules/registration/createCourier";
import authorization from "@/plugins/modules/user/authorization";
import createOrder from "@/plugins/modules/order/createOrder";
import inspectionEmail from "@/plugins/modules/user/inspectionEmail";
import getOrders from "@/plugins/modules/order/getOrders";
import getCouriers from "@/plugins/modules/courier/getCouriers";
import getCouriersHome from "@/plugins/modules/courier/getCouriersHome";
import getOrdersHome from "@/plugins/modules/order/getOrdersHome";
import removeOrder from "@/plugins/modules/order/removeOrder";
import changeOrder from "@/plugins/modules/order/changeOrder";
import changeCourier from "@/plugins/modules/courier/changeCourier";
import removeCourier from "@/plugins/modules/courier/removeCourier";
import getAuthorizedUser from "@/plugins/modules/user/getAuthorizedUser";
import changeCustomer from "@/plugins/modules/customer/changeCustomer";
import removeCustomer from "@/plugins/modules/customer/removeCustomer";
import removeUser from "@/plugins/modules/user/removeUser";

export default createStore({
    modules: {
        createCourier,
        authorization,
        createCustomer,
        createMediaObject,
        createOrder,
        inspectionEmail,
        getOrders,
        getCouriers,
        getCouriersHome,
        getOrdersHome,
        changeOrder,
        removeOrder,
        changeCourier,
        removeCourier,
        getAuthorizedUser,
        changeCustomer,
        removeCustomer,
        removeUser
    }
})